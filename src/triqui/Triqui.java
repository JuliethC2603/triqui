


package triqui;
import java.util.Scanner;
public class Triqui {


	public static void main(String args[]) 
        {
            
		Scanner entrada = new Scanner(System.in);
		int a;
		int b;
		int c;
		int d;
		boolean f;
		String ficha;
		boolean g;
		int i;
		int j = 0;
		int m1[][];
		String m2[][];
		int objetivo;
		int p;
		boolean tj1;
		int turnos;
		int valor;
		m1 = new int[3][3];
		m2 = new String[3][3];
		for (i=0;i<=2;i++) {
			for (j=0;j<=2;j++) {
				m1[i][j] = 0;
				m2[i][j] = " ";
			}
		}
		tj1 = true;
		f = false;
		g = false;
		turnos = 0;
		while (!f) {
			System.out.println(""); // no hay forma directa de borrar la consola en Java
			System.out.println(" ");
			System.out.println("      ||     ||     ");
			System.out.println("   "+m2[0][0]+"  ||  "+m2[0][1]+"  ||  "+m2[0][2]);
			System.out.println("     1||    2||    3");
			System.out.println(" ********************");
			System.out.println("      ||     ||     ");
			System.out.println("   "+m2[1][0]+"  ||  "+m2[1][1]+"  ||  "+m2[1][2]);
			System.out.println("     4||    5||    6");
			System.out.println(" *********************");
			System.out.println("      ||     ||     ");
			System.out.println("   "+m2[2][0]+"  ||  "+m2[2][1]+"  ||  "+m2[2][2]);
			System.out.println("     7||    8||    9");
			System.out.println(" ");
			if (!g && turnos<9) {
				if (tj1) {
					ficha = "X";
					valor = 1;
					objetivo = 1;
					System.out.println("Turno del jugador 1 (X)");
				} else {
					ficha = "O";
					valor = 2;
					objetivo = 8;
					System.out.println("Turno del jugador 2 (O)");
				}
				System.out.println("Ingrese la Posición (1-9):");
				do {
					p = entrada.nextInt();
					if (p<1 || p>9) {
						System.out.println("Posición incorrecta, ingrese nuevamente: ");
						p = 99;
					} else {
						i = (int) Math.floor((p-1)/3);
						j = ((p-1)%3);
						if (m1[i][j]!=0) {
							p = 99;
							System.out.println("Posición incorrecta, ingrese nuevamente: ");
						}
					}
				} while (p==99);
				turnos = turnos+1;
				m1[i][j] = valor;
				m2[i][j] = ficha;
				c = 1;
				d = 1;
				for (i=0;i<=2;i++) {
					a = 1;
					b = 1;
					c = c*m1[i][i];
					d = d*m1[i][2-i];
					for (j=0;j<=2;j++) {
						a = a*m1[i][j];
						b = b*m1[j][i];
					}
					if (a==objetivo || b==objetivo) {
						g = true;
					}
				}
				if (c==objetivo || d==objetivo) {
					g = true;
				} else {
					tj1 = !tj1;
				}
			} else {
				if (g) {
					System.out.println("El Ganador es el: ");
					if (tj1) {
						System.out.println("Jugador 1!");
					} else {
						System.out.println("Jugador 2!");
					}
				} else {
					System.out.println("Empate!");
				}
				f = true;
			}
		}
	}


}


        
